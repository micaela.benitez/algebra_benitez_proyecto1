﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace CustomMath
{
    public struct Vec3 : IEquatable<Vec3>
    {
        #region Variables
        public float x;
        public float y;
        public float z;

        public float sqrMagnitude { get { return Mathf.Pow(magnitude, 2) ; } }
        public Vec3 normalized { get { return new Vec3(x / magnitude, y / magnitude, z / magnitude); } }
        public float magnitude { get { return Mathf.Sqrt(x * x + y * y + z * z); } }
        #endregion

        #region constants
        public const float epsilon = 1e-05f;
        #endregion

        #region Default Values
        public static Vec3 Zero { get { return new Vec3(0.0f, 0.0f, 0.0f); } }
        public static Vec3 One { get { return new Vec3(1.0f, 1.0f, 1.0f); } }
        public static Vec3 Forward { get { return new Vec3(0.0f, 0.0f, 1.0f); } }
        public static Vec3 Back { get { return new Vec3(0.0f, 0.0f, -1.0f); } }
        public static Vec3 Right { get { return new Vec3(1.0f, 0.0f, 0.0f); } }
        public static Vec3 Left { get { return new Vec3(-1.0f, 0.0f, 0.0f); } }
        public static Vec3 Up { get { return new Vec3(0.0f, 1.0f, 0.0f); } }
        public static Vec3 Down { get { return new Vec3(0.0f, -1.0f, 0.0f); } }
        public static Vec3 PositiveInfinity { get { return new Vec3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity); } }
        public static Vec3 NegativeInfinity { get { return new Vec3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity); } }
        #endregion                                                                                                                                                                               y

        #region Constructors
        public Vec3(float x, float y)
        {
            this.x = x;
            this.y = y;
            this.z = 0.0f;
        }

        public Vec3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vec3(Vec3 v3)
        {
            this.x = v3.x;
            this.y = v3.y;
            this.z = v3.z;
        }

        public Vec3(Vector3 v3)
        {
            this.x = v3.x;
            this.y = v3.y;
            this.z = v3.z;
        }

        public Vec3(Vector2 v2)
        {
            this.x = v2.x;
            this.y = v2.y;
            this.z = 0.0f;
        }
        #endregion

        #region Operators
        public static bool operator ==(Vec3 left, Vec3 right)
        {
            float diff_x = left.x - right.x;
            float diff_y = left.y - right.y;
            float diff_z = left.z - right.z;
            float sqrmag = diff_x * diff_x + diff_y * diff_y + diff_z * diff_z;
            return sqrmag < epsilon * epsilon;
        }
        public static bool operator !=(Vec3 left, Vec3 right)
        {
            return !(left == right);
        }

        public static Vec3 operator +(Vec3 leftV3, Vec3 rightV3)
        {
            return new Vec3(leftV3.x + rightV3.x, leftV3.y + rightV3.y, leftV3.z + rightV3.z);
        }

        public static Vec3 operator -(Vec3 leftV3, Vec3 rightV3)
        {
            return new Vec3(leftV3.x - rightV3.x, leftV3.y - rightV3.y, leftV3.z - rightV3.z);
        }

        public static Vec3 operator -(Vec3 v3)
        {
            return new Vec3(-v3.x, -v3.y, -v3.z);
        }

        public static Vec3 operator *(Vec3 v3, float scalar)
        {
            return new Vec3(v3.x*scalar, v3.y*scalar, v3.z*scalar);
        }
        public static Vec3 operator *(float scalar, Vec3 v3)
        {
            return new Vec3(scalar*v3.x, scalar*v3.y, scalar*v3.z);
        }
        public static Vec3 operator /(Vec3 v3, float scalar)
        {
            return new Vec3(v3.x/scalar, v3.y/scalar, v3.z/scalar);
        }

        public static implicit operator Vector3(Vec3 v3)
        {
            return new Vector3(v3.x, v3.y, v3.z);
        }

        public static implicit operator Vector2(Vec3 v2)
        {
            return new Vector2(v2.x, v2.y);
        }
        #endregion

        #region Functions
        public override string ToString()
        {
            return "X = " + x.ToString() + "   Y = " + y.ToString() + "   Z = " + z.ToString();
        }
        public static float Angle(Vec3 from, Vec3 to)
        {
            float dot = Vec3.Dot(from, to);
            float magnitudeFrom = Vec3.Magnitude(from);
            float magnitudeUp = Vec3.Magnitude(to);

            return Mathf.Rad2Deg * (float)(Mathf.Acos(dot / (magnitudeFrom * magnitudeUp)));
        }
        public static Vec3 ClampMagnitude(Vec3 vector, float maxLength)
        {
            float magnitude = Vec3.Magnitude(vector);

            if (magnitude > maxLength)
                return new Vec3((vector.x / magnitude) * maxLength, (vector.y / magnitude) * maxLength, (vector.z / magnitude) * maxLength);
            else
                return vector;
        }
        public static float Magnitude(Vec3 vector)
        {
            return Mathf.Sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
        }
        public static Vec3 Cross(Vec3 a, Vec3 b)
        {
            return new Vec3(a.y * b.z - b.y * a.z, a.z * b.x - b.z * a.x, a.x * b.y - b.x * a.y);
        }
        public static float Distance(Vec3 a, Vec3 b)
        {
            return Mathf.Sqrt(((b.x - a.x) * (b.x - a.x)) + ((b.y - a.y) * (b.y - a.y)) + ((b.z - a.z) * (b.z - a.z)));
        }
        public static float Dot(Vec3 a, Vec3 b)
        {
            return (a.x * b.x + a.y * b.y + a.z * b.z);
        }
        public static Vec3 Lerp(Vec3 a, Vec3 b, float t)
        {
            if (t < 0)
            {
                return a;
            }
            else if (t > 1)
            {
                return b;
            }
            else
            {
                return a + (b - a) * t;
            }
        }
        //   a + (b - a) * t    =    a * (1 - t) + b * t
        public static Vec3 LerpUnclamped(Vec3 a, Vec3 b, float t)
        {
            return a + (b - a) * t;
        }
        public static Vec3 Max(Vec3 a, Vec3 b)
        {
            float max_x;
            float max_y;
            float max_z;

            if (a.x < b.x) max_x = b.x;
            else max_x = a.x;
            if (a.y < b.y) max_y = b.y;
            else max_y = a.y;
            if (a.z < b.z) max_z = b.z;
            else max_z = a.z;

            return new Vec3(max_x, max_y, max_z);
        }
        public static Vec3 Min(Vec3 a, Vec3 b)
        {
            float min_x;
            float min_y;
            float min_z;

            if (a.x > b.x) min_x = b.x;
            else min_x = a.x;
            if (a.y > b.y) min_y = b.y;
            else min_y = a.y;
            if (a.z > b.z) min_z = b.z;
            else min_z = a.z;

            return new Vec3(min_x, min_y, min_z);
        }
        public static float SqrMagnitude(Vec3 vector)
        {
            return Mathf.Pow(Mathf.Sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z), 2);
        }
        public static Vec3 Project(Vec3 vector, Vec3 onNormal) 
        {
            float dot = Vec3.Dot(vector, onNormal);
            float magnitude = Vec3.Magnitude(onNormal);
            float division = dot / (magnitude * magnitude);

            return new Vec3(division * onNormal.x, division * onNormal.y, division * onNormal.z);
        }
        public static Vec3 Reflect(Vec3 inDirection, Vec3 inNormal)
        {
            float dot = Vec3.Dot(inDirection, inNormal);

            return inDirection - (2 * dot) * inNormal;
        }
        public void Set(float newX, float newY, float newZ)
        {
            this = new Vec3(newX, newY, newZ);
        }
        public void Scale(Vec3 scale)
        {
            this = new Vec3(this.x * scale.x, this.y * scale.y, this.z * scale.z);
        }
        public static Vec3 Scale(Vec3 a, Vec3 b)
        {
            return new Vec3(a.x * b.x, a.y * b.y, a.z * b.z);
        }
        public void Normalize()
        {
            this = new Vec3(this.x / Magnitude(this), this.y / Magnitude(this), this.z / Magnitude(this));
        }
        public static Vec3 Normalize(Vec3 vector)
        {
            float magnitude = Vec3.Magnitude(vector);
            return new Vec3(vector.x / magnitude, vector.y / magnitude, vector.z / magnitude);
        }
        #endregion

        #region Internals
        public override bool Equals(object other)
        {
            if (!(other is Vec3)) return false;
            return Equals((Vec3)other);
        }

        public bool Equals(Vec3 other)
        {
            return x == other.x && y == other.y && z == other.z;
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() ^ (y.GetHashCode() << 2) ^ (z.GetHashCode() >> 2);
        }
        #endregion
    }
}