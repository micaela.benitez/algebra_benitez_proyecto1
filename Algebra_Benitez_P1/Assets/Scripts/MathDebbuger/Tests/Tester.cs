﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MathDebbuger;
using CustomMath;
public class Tester : MonoBehaviour
{
    void Start()
    {
        List<Vector3> vectors = new List<Vector3>();
        vectors.Add(new Vec3(10.0f, 0.0f, 0.0f));
        vectors.Add(new Vec3(10.0f, 10.0f, 0.0f));
        vectors.Add(new Vec3(20.0f, 10.0f, 0.0f));
        vectors.Add(new Vec3(20.0f, 20.0f, 0.0f));
        Vector3Debugger.AddVectorsSecuence(vectors, false, Color.red, "secuencia");
        Vector3Debugger.EnableEditorView("secuencia");
        Vector3Debugger.AddVector(new Vector3(10, 10, 0), Color.blue, "elAzul");
        Vector3Debugger.EnableEditorView("elAzul");
        Vector3Debugger.AddVector(Vector3.down * 7, Color.green, "elVerde");
        Vector3Debugger.EnableEditorView("elVerde");

        //Vector3 vec3A = new Vector3(1,5,3);
        //Vector3 vec3B = new Vector3(2,3,5);
        //Vec3 miVec3A = new Vec3(1,5,3);
        //Vec3 miVec3B = new Vec3(2,3,5);
        //float num = 5;
        //float miNum = 5;

        //Debug.Log("01 - Angle: Vector3: " + Vector3.Angle(vec3A, vec3B) + " y Vec3: " + Vec3.Angle(miVec3A, miVec3B));
        //Debug.Log("02 - ClampMagnitude: Vector3: " + Vector3.ClampMagnitude(vec3A, num) + " y Vec3: " + Vec3.ClampMagnitude(miVec3A, miNum));
        //Debug.Log("03 - Magnitude: Vector3: " + Vector3.Magnitude(vec3A) + " y Vec3: " + Vec3.Magnitude(miVec3A));
        //Debug.Log("04 - Cross: Vector3: " + Vector3.Cross(vec3A, vec3B) + " y Vec3: " + Vec3.Cross(miVec3A, miVec3B));
        //Debug.Log("05 - Distance: Vector3: " + Vector3.Distance(vec3A, vec3B) + " y Vec3: " + Vec3.Distance(miVec3A, miVec3B));
        //Debug.Log("06 - Dot: Vector3: " + Vector3.Dot(vec3A, vec3B) + " y Vec3: " + Vec3.Dot(miVec3A, miVec3B));
        //Debug.Log("07 - Lerp: Vector3: " + Vector3.Lerp(vec3A, vec3B, num) + " y Vec3: " + Vec3.Lerp(miVec3A, miVec3B, miNum));
        //Debug.Log("08 - LerpUnclamped: Vector3: " + Vector3.LerpUnclamped(vec3A, vec3B, num) + " y Vec3: " + Vec3.LerpUnclamped(miVec3A, miVec3B, miNum));
        //Debug.Log("09 - Max: Vector3: " + Vector3.Max(vec3A, vec3B) + " y Vec3: " + Vec3.Max(miVec3A, miVec3B));
        //Debug.Log("10 - Min: Vector3: " + Vector3.Min(vec3A, vec3B) + " y Vec3: " + Vec3.Min(miVec3A, miVec3B));
        //Debug.Log("11 - SqrMagnitude: Vector3: " + Vector3.SqrMagnitude(vec3A) + " y Vec3: " + Vec3.SqrMagnitude(miVec3A));
        //Debug.Log("12 - Project: Vector3: " + Vector3.Project(vec3A, vec3B) + " y Vec3: " + Vec3.Project(miVec3A, miVec3B));
        //Debug.Log("13 - Reflect: Vector3: " + Vector3.Reflect(vec3A, vec3B) + " y Vec3: " + Vec3.Reflect(miVec3A, miVec3B));
        //Debug.Log("14 - Scale: Vector3: " + Vector3.Scale(vec3A, vec3B) + " y Vec3: " + Vec3.Scale(miVec3A, miVec3B));
        //Debug.Log("15 - Normalize: Vector3: " + Vector3.Normalize(vec3A) + " y Vec3: " + Vec3.Normalize(miVec3A));
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            StartCoroutine( UpdateBlueVector());
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            Vector3Debugger.TurnOffVector("elAzul");
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Vector3Debugger.TurnOnVector("elAzul");
        }
    }

    IEnumerator UpdateBlueVector()
    {
        for (int i = 0; i < 100; i++)
        {
            Vector3Debugger.UpdatePosition("elAzul", new Vector3(2.4f, 6.3f, 0.5f) * (i * 0.05f));
            yield return new WaitForSeconds(0.2f);
        }
    }

}
